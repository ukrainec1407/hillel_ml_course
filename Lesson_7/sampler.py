import pandas as pd
#import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
#import seaborn as sns
#rom imblearn.under_sampling import CondensedNearestNeighbour
#from sklearn.datasets import make_classification
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import SMOTE

df = pd.read_csv('Data_sampler/glass.data')

df.columns = ['Id number', 'RI', 'Na', 'Mg', "Al", "Si", "K", "Ca", "Ba", "Fe", "Type of glass"]

#print(df.isna().sum())
#пустых строк нету

#Accuracy score: 0.7717391304347826
df.drop(df[(df['K'] > 2)].index, inplace=True)
df.drop(df[(df['Ba'] > 2)].index, inplace=True)
df.drop(df[(df['Fe'] > 0.4)].index, inplace=True)
df.drop(df[(df['Al'] > 3)].index, inplace=True)
df.drop(df[(df['Na'] > 16)].index, inplace=True)
df.drop(df[(df['RI'] > 1.53)].index, inplace=True)
#Accuracy score: 0.7865168539325843
df = df.drop(columns=['Id number'])
#они записали все по порядку, потому id сильно коррелирует с таргетом. 

scaler = StandardScaler()
df = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)

encoder = LabelEncoder()
df['Type of glass'] = encoder.fit_transform(df['Type of glass'])

"""
sns.pairplot(
    df[df.columns.values]
)
plt.show()
"""

#разброс у таргета: минимальная колонка - 10 записей, максимальная 70. 
#Учитывая что там 6 типов, можно попробовать построить модель при 60-65 записях.
#Попробую UnderSampler, посмотрю что выйдет

"""
X = df.drop('Type of glass', axis=1)
y = df['Type of glass']

cnn = CondensedNearestNeighbour(n_neighbors=9, random_state=42)
X_resampled, y_resampled = cnn.fit_resample(X, y)

X_train, X_test, y_train, y_test = train_test_split(X_resampled, y_resampled, test_size=0.2, random_state=42)
"""
#Получилось только хуже, чем если вообще не использовать Sampler

#тогда попробую OverSampler, SMOTE алгоритм

X_orig = df.drop(columns=['Type of glass'])
y_orig = df['Type of glass']

smote = SMOTE(random_state=42)
X_resampled, y_resampled = smote.fit_resample(X_orig, y_orig)

X_train, X_test, y_train, y_test = train_test_split(X_resampled, y_resampled, test_size=0.2, random_state=42)

#target = 'Type of glass'
#features = df.drop(columns=['Type of glass']).columns
#X_train, X_test, y_train, y_test = train_test_split(df[features], df[target], test_size=0.2, random_state=42)

clf = LogisticRegression(random_state=42).fit(X_train, y_train)

y_pred = clf.predict(X_test)

print(y_test[:15])
print(y_pred[:15])

accuracy = accuracy_score(y_test, y_pred)
print('LogisticRegression Accuracy score:', accuracy)

#Accuracy score: 0.7865168539325843, OverSampler SMOTE алгоритм улучшил результат значительно. 
#Без OverSampler - Accuracy score: 0.6829268292682927


