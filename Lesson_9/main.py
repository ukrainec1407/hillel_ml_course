import pandas as pd
from sklearn.cluster import KMeans
from clustergram import Clustergram
import matplotlib.pyplot as plt

#import hdbscan

#from googletrans import Translator 

df = pd.read_csv('Data/SouthGermanCredit.asc')

"""
df = pd.read_csv('Data/SouthGermanCredit.asc', sep=' ')
print(df.columns)

translator = Translator() 
translated_columns = [translator.translate(col, src='de', dest='en').text for col in df.columns]

df.columns = translated_columns
df.to_csv('Data/SouthGermanCredit.asc', index=False, header=translated_columns, encoding='utf-8')

df = pd.read_csv('Data/SouthGermanCredit.asc')"""

X = df.drop(columns=['credit']) 
y = df['credit']

cgram = Clustergram(range(1, 8))
cgram.fit(X)
cgram.plot()

plt.show()

kmeans = KMeans(n_clusters=4, random_state=42)

kmeans.fit(X)

print(kmeans.labels_)





