import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import preprocessing


pd.set_option('display.max_columns', None)

df = pd.read_csv('Seasons_Stats.csv')
#df.info(memory_usage=True) 10+MB

df.dropna(axis=1, how='all', inplace=True)
df.dropna(axis=0, how='any', inplace=True)


df ['Year'] = df.Year.astype('int16')
df ['Player'] = df.Player.astype('string')
df ['Pos'] = df.Pos.astype('string')
df ['Age'] = df.Age.astype('int16')
df ['Tm'] = df.Tm.astype('string')
df ['G'] = df.G.astype('int16')
df ['GS'] = df.GS.astype('int16')
df ['MP'] = df.MP.astype('int32')
df ['PER'] = df.PER.astype('float16')
df ['FTr'] = df.FTr.astype('float16')
df ['OWS'] = df.OWS.astype('float16')
df ['DWS'] = df.DWS.astype('float16')
df ['WS'] = df.WS.astype('float16')
df ['OBPM'] = df.OBPM.astype('float16')

#df.info(memory_usage=True)4.9+MB

df[['MP'] + ["TS%"]].groupby(
    "MP"
).mean().plot()
#Чем больше игрок играет минут - тем более у него эффективные броски мяча

team_age_df = df.groupby(['Tm'])['Age'].mean().reset_index()
fig, ax = plt.subplots()
ax.bar(team_age_df['Tm'], team_age_df['Age'])
#Нету ярко выраженныж возрастных отличий игроков в разных


sns.displot(df[['TS%']])
#True shooting percentage выглядит как нормальное распределение

sns.jointplot(x='Age', y='MP', data=df, kind='scatter')
#Правый верхний угол слизан, значит более старшие игроки меньше часов проводят в игре

def get_zodiac_sign(birthdate):
    day, month = birthdate.day, birthdate.month
    if (month == 12 and day >= 22) or (month == 1 and day <= 19):
        return "Козерог"
    elif (month == 1 and day >= 20) or (month == 2 and day <= 18):
        return "Водолей"
    elif (month == 2 and day >= 19) or (month == 3 and day <= 20):
        return "Рыбы"
    elif (month == 3 and day >= 21) or (month == 4 and day <= 19):
        return "Овен"
    elif (month == 4 and day >= 20) or (month == 5 and day <= 20):
        return "Телец"
    elif (month == 5 and day >= 21) or (month == 6 and day <= 20):
        return "Близнецы"
    elif (month == 6 and day >= 21) or (month == 7 and day <= 22):
        return "Рак"
    elif (month == 7 and day >= 23) or (month == 8 and day <= 22):
        return "Лев"
    elif (month == 8 and day >= 23) or (month == 9 and day <= 22):
        return "Дева"
    elif (month == 9 and day >= 23) or (month == 10 and day <= 22):
        return "Весы"
    elif (month == 10 and day >= 23) or (month == 11 and day <= 21):
        return "Скорпион"
    elif (month == 11 and day >= 22) or (month == 12 and day <= 21):
        return "Стрелец"

df_player_data = pd.read_csv('player_data.csv')

df_player_data['birth_date'] = pd.to_datetime(df_player_data['birth_date'], format='%B %d, %Y')

df_player_data['get_zodiac_sign'] = df_player_data['birth_date'].apply(get_zodiac_sign)
df_player_data.rename(columns={'name': 'Player'}, inplace=True)


result_frame = pd.merge(df, df_player_data, 
how="inner", on=["Player"])
result_frame = result_frame.fillna('')

#result_frame.info(memory_usage=True)

team_zodiac_df = result_frame.groupby(['get_zodiac_sign'])['Age'].mean().reset_index()
fig, ax = plt.subplots()
ax.bar(team_zodiac_df['get_zodiac_sign'], team_zodiac_df['Age'])
#Добавили новую фичу "Знак задиака игроков. И еще раз подтвердили что твой знак задиака ни на что не влияет)"

plt.show()