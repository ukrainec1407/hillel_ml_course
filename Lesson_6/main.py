import pandas as pd
#import seaborn as sns
#import matplotlib.pyplot as plt
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.linear_model import Lasso
from sklearn.metrics import mean_absolute_error
from sklearn.linear_model import Ridge
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.preprocessing import LabelEncoder

df = pd.read_csv('Data/abalone.data')

df.columns = ['Sex', 'Length', 'Diameter', "Height", "Whole_weight", "Shuced_weight", "Viscera_weigt", "Shell_weight", "Rings"]

pd.set_option('display.max_columns', None)

encoder = LabelEncoder()

df['Sex'] = encoder.fit_transform(df['Sex'])

df ['Diameter'] = df.Diameter.astype('float16')
df ['Height'] = df.Height.astype('float16')
df ['Whole_weight'] = df.Whole_weight.astype('float16')
df ['Rings'] = df.Rings.astype('int16')

df['Circumference'] = (np.pi * df['Diameter'])

df['Weight_to_size_ratio'] = (df['Whole_weight'] / df['Circumference'])


df = df.drop(columns=['Sex'])
df = df.drop(columns=['Diameter'])
df = df.drop(columns=['Viscera_weigt'])
df = df.drop(columns=['Height'])
df = df.drop(columns=['Whole_weight'])

rings = df['Rings']
scaler = StandardScaler()
df = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)
df['Rings'] = rings

target = 'Rings'
features = df.drop(columns=['Rings']).columns

X_train, X_test, y_train, y_test = train_test_split(df[features], df[target], test_size=0.2, random_state=42)

#----------------------Lasso------------------

lasso = Lasso(alpha=0.0001)
lasso.fit(X_train, y_train)

print("Lasso Coeffs:")
for feature, coef in zip(df.drop(columns=['Rings']).columns, lasso.coef_):
    print(f"{feature}: {coef:.4f}")

y_pred = lasso.predict(X_test)

y_pred = np.round(y_pred)

print(y_test[:15])
print(y_pred[:15])

accuracy = accuracy_score(y_test, y_pred)
print('lasso Accuracy score:', accuracy)

#-----------LogisticRegression----------------
"""
hyperparameters = {
    'penalty': ['l1', 'l2'],
    'C': [250, 500 ,1000, 5000]
}

model = LogisticRegression()

grid_search = GridSearchCV(model, hyperparameters, cv=5)

grid_search.fit(X_train, y_train)

print('Best penalty:', grid_search.best_estimator_.get_params()['penalty'])
print('Best C:', grid_search.best_estimator_.get_params()['C'])
"""

model = LogisticRegression(max_iter=1000, penalty='l2', C=1000)

model.fit(X_train, y_train)

y_pred = model.predict(X_test)

print(y_test[:15])
print(y_pred[:15])

accuracy = accuracy_score(y_test, y_pred)
print('LogisticRegression Accuracy score:', accuracy)

#-----------LogisticRegression----------------
#----------------------RandomForestRegressor------------------
"""
rf = RandomForestRegressor()

parameters = {
    'n_estimators': [100, 150, 200],
    'max_features': ['auto', 'sqrt', 'log2'],
    'max_depth':[0, 5],
    'bootstrap': [True, False]
}

grid_search = GridSearchCV(estimator = rf, param_grid = parameters, 
                          cv = 5, n_jobs = -1, verbose = 2)
#Best parameters:  {'bootstrap': True, 'max_depth': 5, 'max_features': 'auto', 'n_estimators': 100}

grid_search.fit(X_train, y_train)

# выводим лучшие параметры и метрику на кросс-валидации
print('Best parameters: ', grid_search.best_params_)
print('Best cross-validation score: {:.2f}'.format(grid_search.best_score_))
"""

rf = RandomForestRegressor(bootstrap=True, max_depth=5, max_features='auto', n_estimators=100)

rf.fit(X_train, y_train)

y_pred = rf.predict(X_test)

y_pred = np.round(y_pred)

print(y_test[:15])
print(y_pred[:15])

accuracy = accuracy_score(y_test, y_pred)
print('RandomForestRegressor Accuracy score:', accuracy)

