import numpy as np
import pandas as pd

#---------------setup---------------
pd.set_option('display.max_columns', 100)
pd.set_option('display.max_rows', 100)
pd.set_option('display.precision', 3)

data = pd.read_csv('adult.data.csv')
tasks = []
#---------------setup end---------------


#1. How many men and women are represented in this dataset?
tasks.append(data['sex'].value_counts(dropna=True))

#2. What is the average age of women?

tasks.append(f"{data[(data['sex'] == 'Female')]['age'].mean()} y.o.")

#3. Count the number of people with native-country as Germany
germans = data[data['native-country'] == 'Germany']
tasks.append(f"{(len(germans) / len(data) * 100).__round__(1)}%")

#4-5.What are the mean values and standard deviations of the ages of those
#who receive more than 50K per year (the *salary* trait) and those who receive less than 50K per year?

tasks.append(data.groupby(by='salary')['age'].std())
tasks.append(data.groupby(by='salary')['age'].mean())

#6. Is it true that people who earn more than 50k 
#have at least a higher education? 

#I also found it interesting to study the relationship based on age and gender
age = 25

sex = ['Female']
#sex = ['Male']
#sex = ['Male', 'Female']

#Code groups data by salary and education columns.
just_data = data[(data['sex'].isin(sex)) & (data['age'] < age)].groupby(['salary', 'education'])['education'].count()

#List of high education levels for grouping data.
high_education = ['Doctorate', 'Bachelors', 'Prof-school', 'Assoc-acdm','Assoc-voc', 'Masters']

#Count of people with >50K salary and high education.
mor_then_50_with_high_ed = data[(data['sex'].isin(sex)) & (data['age'] < age) & (data['salary'] == '>50K') & (data['education'].isin(high_education))]['education'].count()
mor_then_50 = data[(data['sex'].isin(sex)) & (data['age'] < age) & (data['salary'] == '>50K')]['education'].count()

#Calculating percentage of people with high salary and education.
mor_then_50_with_high_ed = (mor_then_50_with_high_ed / mor_then_50) * 100

#Count of people with <=50K salary and high education.
less_then_50_with_high_ed = data[(data['sex'].isin(sex)) & (data['age'] < age) & (data['salary'] == '<=50K') & (data['education'].isin(high_education))]['education'].count()
less_then_50 = data[(data['sex'].isin(sex)) & (data['age'] < age) & (data['salary'] == '<=50K')]['education'].count()

#Calculating percentage of people with low salary and education.
less_then_50_with_high_ed = (less_then_50_with_high_ed / less_then_50) * 100

tasks.append(f"""{mor_then_50_with_high_ed.round(1)}% of {sex} in {age} y/o, who earn more than 50 000/year have high education
while
{less_then_50_with_high_ed.round(1)}% of {sex} in {age} y/o, who earn less than 50 000/year have high education""")

#No, it is not necessarily true that people who earn more than 
#50k have at least a higher education.

#7. Print the age statistics for each race for each gender.
#Find the maximum age of male individuals of the Amer-Indian-Eskimo race

max_age = data[(data['race'] == 'Amer-Indian-Eskimo') & (data['sex'] == 'Male')]['age'].max()

tasks.append(f"{data.groupby(['race', 'sex'])['age'].describe(percentiles=[0.5])} \n maximum age of male individuals of the Amer-Indian-Eskimo race: {max_age}")

#8. Among whom is the proportion of those who earn a lot (>50K) greater: 
#among married or unmarried men

more_than_50_married = data[(data['sex'] == "Male") & (data['salary'] == '>50K') & (data['marital-status'].str.startswith('Married'))]['age'].count()
more_then_50 = data[(data['sex'] == "Male") & (data['salary'] == '>50K')]['age'].count()

more_then_50_married = ((more_than_50_married / more_then_50) * 100).round(1)

less_than_50_married = data[(data['sex'] == "Male") & (data['salary'] == '<=50K') & ~(data['marital-status'].str.startswith('Married'))]['age'].count()
less_then_50 = data[(data['sex'] == "Male") & (data['salary'] == '<=50K')]['age'].count()

less_then_50_married = ((less_than_50_married / less_then_50) * 100).round(1)

tasks.append(f"{more_then_50_married}% of men, who earn more then 50K$ are married, while {less_then_50_married}% of men, who earn less then 50K$ are single")

#9. What is the maximum number of working hours per week? 
#How many people work such number of hours, and what is the percentage of those people who earn a lot?

#find maximum working hours per week
max_work_hours = data['hours-per-week'].max()
task_9 = (f"max working time per week: {max_work_hours}\n")

#find count of workers working max hours/week.
overworkers_time = data['hours-per-week'].value_counts(normalize=True).max()*100
task_9 += (f"amount of workers who works {max_work_hours}h/week: is {overworkers_time}%\n")

#Calculating percentage of high earners working max hours/week.
rich_overworkers = data[(data['hours-per-week'] == max_work_hours)]['salary'].value_counts(normalize=True)*100
task_9 += (f"amount of rich workers who works {max_work_hours}h/week: is {rich_overworkers[1]}%")

tasks.append(task_9)

#10. Calculate the average working hours 
#for low and high earners in each country.

tasks.append(data.groupby(['salary', 'native-country'])['hours-per-week'].mean())

print("*****HW_3, student Ihor Shybka********")

while True:
    num = 0
    while True:
        num_str = input("Choose one of task you want to check, by writing a number")
        try:
            num = int(num_str)
            if num < 1 or num > 10:
                print("Number must be between 1 and 14.")
            else:
                break
        except ValueError:
            print("Invalid input, please enter a number between 1 and 14.")

    print("")
    print("---------------------------------------------------------------------")
    print("")

    print(tasks[num - 1])

    print("")
    print("---------------------------------------------------------------------")
    print("")

    print("to exit press 'ctrl + c'")