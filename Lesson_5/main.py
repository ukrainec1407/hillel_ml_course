import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn.linear_model import SGDRegressor
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import GridSearchCV


df = pd.read_csv('data\winequality-white.csv', sep=";")

#pd.set_option('display.max_columns', None)
#print(df.head())
#print(df.isna().sum())
#пустых строк нету, как и категориальных фич

"""sns.pairplot(
    df[df.columns.values[:4]]
)
sns.pairplot(
    df[df.columns.values[4:8]]
)
sns.pairplot(
    df[df.columns.values[8:]]
)"""

#выбивающиеся данные:
#density > 1.01
#residual sugar > 60
#free sulfur dioxide > 200

df.drop(df[(df['density'] > 1.01)].index, inplace=True)
df.drop(df[(df['residual sugar'] > 60)].index, inplace=True)
df.drop(df[(df["free sulfur dioxide"] > 200)].index, inplace=True)

df['flowability'] = df['density'] / df['pH']
df['alcohol / quality'] = df['alcohol'] / df['quality']
df = df.drop('density', axis=1)


#corr = df.corr()

#sns.heatmap(corr, cmap="YlGnBu")

#plt.show()

scaler = StandardScaler()
df = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)

# Разбиение датафрейма на тренировочную и тестовую выборки
train_val, test = train_test_split(df, test_size=0.15, random_state=42)
train, val = train_test_split(train_val, test_size=0.15, random_state=42)

X_train = train.drop('quality', axis=1)
y_train = train['quality']

X_val = val.drop('quality', axis=1)
y_val = val['quality']

X_test = test.drop('quality', axis=1)
y_test = test['quality']

# Создание модели, которую требуется оптимизировать
model = SGDRegressor()
model = model.fit(X_train, y_train)
predict = model.predict(X_val)

r2 = r2_score(y_val, predict)
mse = mean_squared_error(y_val, predict)

print('Mean Squared Error  of Basic model:', mse)
print(f"r2 of Basic model {r2}")

# Сетка гиперпараметров для перебора
param_grid = {'alpha': [0.001, 0.01, 0.1, 1], 
              'l1_ratio': [0, 0.25, 0.5, 0.75, 1], 
              'max_iter': [1000, 5000, 10000], 
              'tol': [1e-5, 1e-7, 1e-10, 1e-12]}

# Объект GridSearchCV
grid_search = GridSearchCV(estimator=model, param_grid=param_grid, cv=5)

# Обучение модели на тренировочной выборке с перебором гиперпараметров
grid_search.fit(X_train, y_train)

# Печать лучших гиперпараметров и наилучшего значения качества на валидации
print(grid_search.best_params_)
print(grid_search.best_score_)

# Вывод наилучшей модели
best_model = grid_search.best_estimator_
print(best_model)

best_model.fit(X_train, y_train)

# делаем прогноз на тестовых данных
y_pred = best_model.predict(X_val) 

# считаем метрики на тестовых данных
mse = mean_squared_error(y_val, y_pred)
r2 = r2_score(y_val, y_pred)

print('Mean Squared Error:', mse)
print('R^2 Score:', r2)

y_pred = best_model.predict(X_test) 

print(y_pred[:15])
print(y_test[:15])