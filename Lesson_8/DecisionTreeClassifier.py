import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import GridSearchCV

#import matplotlib.pyplot as plt
#import seaborn as sns
#from sklearn.preprocessing import StandardScaler

df = pd.read_csv('Data/crx.data')

df.columns = [f'A{i+1}' for i in range(16)]

encoder = LabelEncoder()

data_to_encode = ["A1", "A4", "A5", "A6", "A7", "A9", "A10", "A12", "A13", "A16"]

df = df[~df.apply(lambda x: x.astype(str).str.contains('\?')).any(1)]


for col in data_to_encode:
    df[col] = encoder.fit_transform(df[col])

#0.79
poly = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly.fit_transform(df)

df = pd.DataFrame(X_poly, columns = poly.get_feature_names_out(df.columns))
#0.99

"""
print(df.nunique())
A1: b, a. 2
A4: u, u, l, l. 4
A5: g, p, gg. 3 
A6: c, d, cc, i, j, k, m, r, q, w, x, e, aa, ff. 14
A7: v, h, bb, j, n, z, dd, ff, o. 9
A9: t, f. 2
A10: t, f. 2
A12: t, f. 2 
A13: g, p, s. 3
A16: +,- (class attribute) 2
"""

"""
scaler = StandardScaler()
df = pd.DataFrame(scaler.fit_transform(df), columns=df.columns)

sns.pairplot(
    df[df.columns.values[:5]]
)

sns.pairplot(
    df[df.columns.values[5:10]]
)

sns.pairplot(
    df[df.columns.values[10:]]
)
plt.show()
No outliers among the data
"""
X = df.drop(columns=['A16'])
y = df['A16']

X_train, X_test, y_trin, y_test = train_test_split(X, y, test_size= 0.33, random_state=24)

#************BASIC DecisionTreeClassifier**************

clf = DecisionTreeClassifier(random_state=42).fit(X_train, y_trin)
y_pred = clf.predict(X_test)

print(y_test[:25])
print(y_pred[:25])

accuracy = accuracy_score(y_test, y_pred)
print('Basic Accuracy score:', accuracy)
#Basic Accuracy score: 1.0


#************GridSearchCV DecisionTreeClassifier**************

"""
clf = DecisionTreeClassifier()

param_grid = {
    'criterion' : ["gini", "entropy", "log_loss"],
    'max_depth': [1, 2, 5, 10],
    'min_samples_split': [1, 2, 3, 4],
    'min_samples_leaf': [1, 2, 3],
    'min_weight_fraction_leaf' : [0, 0.1, 0,7, 1],
    'max_features' : [1, 3, 5, 'auto', 'sqrt', 'log2']
}

grid_search = GridSearchCV(clf, param_grid, cv=5)


grid_search.fit(X_train, y_trin)

print("Best parameters: ", grid_search.best_params_)
print("Best score: ", grid_search.best_score_)


#Best parameters:  
#{'criterion': 'gini', 'max_depth': 5, 'max_features': 'auto', 'min_samples_leaf': 1, 'min_samples_split': 2, 'min_weight_fraction_leaf': 0}
"""

#************Best DecisionTreeClassifier**************

clf = DecisionTreeClassifier(criterion='gini', max_depth=5, max_features='auto', min_samples_leaf=1, min_samples_split=2, 
min_weight_fraction_leaf=0, random_state=42)

clf.fit(X_train, y_trin)
y_pred = clf.predict(X_test)

print(y_test[:15])
print(y_pred[:15])

accuracy = accuracy_score(y_test, y_pred)
print('Advanced Accuracy score:', accuracy)
#Advanced Accuracy score: 0.9953703703703703
