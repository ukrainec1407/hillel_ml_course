import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import PolynomialFeatures

df = pd.read_csv('Data/crx.data')

df.columns = [f'A{i+1}' for i in range(16)]

encoder = LabelEncoder()

data_to_encode = ["A1", "A4", "A5", "A6", "A7", "A9", "A10", "A12", "A13", "A16"]

df = df[~df.apply(lambda x: x.astype(str).str.contains('\?')).any(1)]

for col in data_to_encode:
    df[col] = encoder.fit_transform(df[col])

#Accuracy score: 0.8549618320610687
poly = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly.fit_transform(df)

df = pd.DataFrame(X_poly, columns = poly.get_feature_names_out(df.columns))
#Accuracy score: 1.0

X = df.drop(columns=['A16'])
y = df['A16']

X_train, X_test, y_trin, y_test = train_test_split(X, y, test_size= 0.33, random_state=24)

clf_rf = RandomForestClassifier(max_depth=2, random_state=42).fit(X_train, y_trin)

y_pred = clf_rf.predict(X_test)

print(list(zip(y_pred, y_test)))

accuracy = accuracy_score(y_test, y_pred)
print('Accuracy score:', accuracy)
#Accuracy score: 1.0

