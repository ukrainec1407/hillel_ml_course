import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
import lightgbm as lgb
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import PolynomialFeatures

df = pd.read_csv('Data/crx.data')

df.columns = [f'A{i+1}' for i in range(16)]

encoder = LabelEncoder()

data_to_encode = ["A1", "A4", "A5", "A6", "A7", "A9", "A10", "A12", "A13", "A16"]

df = df[~df.apply(lambda x: x.astype(str).str.contains('\?')).any(1)]

for col in data_to_encode:
    df[col] = encoder.fit_transform(df[col])

#Accuracy score: 0.8549618320610687
poly = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly.fit_transform(df)

df = pd.DataFrame(X_poly, columns = poly.get_feature_names_out(df.columns))
#Accuracy score: 1.0

X = df.drop(columns=['A16'])
y = df['A16']

X_train, X_test, y_trin, y_test = train_test_split(X, y, test_size= 0.33, random_state=24)

train_data = lgb.Dataset(X_train, label=y_trin)

params = {
    'boosting_type': 'dart',
    'num_leaves': 30,
    'learning_rate': 0.1,
    'feature_fraction': 0.9,
    'bagging_fraction': 0.8,
    'bagging_freq': 5,
    'verbose': 0,
    'force_row_wise':'true',
    'num_boost_round':100
}

boosting_model = lgb.train(params, train_data, num_boost_round=100)

y_pred = boosting_model.predict(X_test)
y_pred = [1 if pred >= 0.5 else 0 for pred in y_pred]

print(list(zip(y_pred, y_test)))

accuracy = accuracy_score(y_test, y_pred)
print("Accuracy:", accuracy)
#Accuracy: 1