import pandas as pd
from prophet import Prophet
import matplotlib.pyplot as plt

df = pd.read_csv('Data/T10Y2Y.csv', parse_dates=True)
df.columns = ['ds', 'y']
print(df.head())
df['y'] = pd.to_numeric(df['y'], errors='coerce').astype('float16')
df = df.interpolate()
df.info()

split_day = -365

# Разделите данные на тренировочную и тестовую выборки
train_data = df[:split_day]

model = Prophet(seasonality_mode='additive', growth='linear')
model.fit(train_data)

future = model.make_future_dataframe(abs(split_day), freq='D')

forecast = model.predict(future)[['ds', 'yhat']].set_index('ds')
plt.plot(forecast, label='predict')
df.plot()

plt.show()

